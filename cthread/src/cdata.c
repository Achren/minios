/* 	Arquivo de Código do Gerenciador de Threads e Escalonador
	 	Implementado por Camila Primieri e Roberta Robert
*/

#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>
#include "../include/support.h"
#include "../include/cdata.h"

#define _XOPEN_SOURCE 600