/* 	
Arquivo de Código da Biblioteca cthread.c 
Implementado por Camila Primieri e Roberta Robert
Sistemas Operacionais I - N
Universidade Federal do Rio Grande do Sul - UFRGS
*/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <ucontext.h>
#include <string.h>
#include "../include/support.h"
#include "../include/cthread.h"
#include "../include/cdata.h"

#define _XOPEN_SOURCE 600
#define SUCESSO 0
#define ERRO -1
#define CRIACAO 0
#define APTO 1
#define EXECUCAO 2
#define BLOQUEADO 3
#define TERMINO 4

/* 
Globais 
*/

int _inicia = 0; 					// Controle da inicialização da thread main
int _next_tid; 						// Próximo tid
FILA2 aptos; 							// Fila de aptos
FILA2 terminados;					// Fila de threads encerradas
FILA2 bloqueados; 				// Fila de bloqueados
FILA2 duplas_bloq;				// Fila das duplas bloqueados/bloqueador
TCB_t* actual_thread; 		// Thread em execução
ucontext_t* final_thread; // Contexto para o término de uma thread 
ucontext_t* dispatcher_c; // Contexto para o dispacher

/* 
Funções Auxiliares 
*/
int testa_inicializacao();
int cria_thread_main();
void dispatcher();
int encerra_thread();
int existe_tid(int tid);
int ja_bloqueado(int tid);
int desbloqueia(int tid);

int testa_inicializacao(){
	
	/* Se é a primeira vez executando, vai ter que criar a thread main e as filas */
	if(_inicia == 0){
		_next_tid = cria_thread_main();
		
		if(_next_tid == 1){
			_inicia = 1;
		} 
		else{
			return ERRO;
		}

		CreateFila2(&aptos);
		CreateFila2(&terminados);
		CreateFila2(&bloqueados);
		CreateFila2(&duplas_bloq);

		/* Cria o contexto para terminar uma thread */
		final_thread = (ucontext_t*) malloc(sizeof(ucontext_t));
		getcontext(final_thread);
		final_thread->uc_stack.ss_sp = (char*) malloc(SIGSTKSZ);
    final_thread->uc_stack.ss_size = SIGSTKSZ;
    makecontext(final_thread, (void (*)(void)) encerra_thread, 0, NULL);

    /* Cria o contexto do dispatcher */
		dispatcher_c = (ucontext_t*) malloc(sizeof(ucontext_t));
		getcontext(dispatcher_c);
		dispatcher_c->uc_stack.ss_sp = (char*) malloc(SIGSTKSZ);
    dispatcher_c->uc_stack.ss_size = SIGSTKSZ;
    makecontext(dispatcher_c, (void (*)(void)) dispatcher, 0, NULL);

	}
	return SUCESSO;	
}

int cria_thread_main(){

	/* Cria um TCB para a thread main */
	TCB_t * tcb_main = (TCB_t *) malloc(sizeof(TCB_t));
	if(tcb_main == NULL){
		printf("Erro na criação da thread main.\n");
		return ERRO;
	}
	else{
		tcb_main->tid = 0; 					// recebe tid = 0				
		tcb_main->state = EXECUCAO;			// estado de execução
		tcb_main->ticket = (Random2() % 256);	// ticket da loteria
	}

	actual_thread = tcb_main;

	return 1;
}

void dispatcher(){
	int sorted_ticket, winner_ticket = 255, distance;
	TCB_t* winner_thread = NULL;
	TCB_t* aux = NULL;

	sorted_ticket = (Random2() % 256);

	printf("ticket sorteado: %d\n", sorted_ticket);

	FirstFila2(&aptos);
	aux = GetAtIteratorFila2(&aptos);
	
	while(aux != NULL){

		printf("thread na lista de aptos tid #%d, ticket #%d\n", aux->tid, aux->ticket);
		distance = abs(aux->ticket - sorted_ticket);
		if(distance < winner_ticket){
			winner_ticket = distance;
			winner_thread = aux;
		} 
		else if(distance == winner_ticket){
			if(aux->tid < winner_thread->tid){
				winner_ticket = distance;
				winner_thread = aux;
			}
		}
		NextFila2(&aptos);
		aux = GetAtIteratorFila2(&aptos);
	}

	printf("thread vencedora tid #%d, ticket #%d\n", winner_thread->tid, winner_thread->ticket);
	
	FirstFila2(&aptos);
	aux = GetAtIteratorFila2(&aptos);
	while(aux != NULL){

		if(aux->tid == winner_thread->tid){
			DeleteAtIteratorFila2(&aptos);
			break;
		} 
		NextFila2(&aptos);
		aux = GetAtIteratorFila2(&aptos);
	}

	winner_thread->state = EXECUCAO;
	actual_thread = winner_thread;
	setcontext(&winner_thread->context);
}

int encerra_thread(){

	actual_thread->state = TERMINO;

	info_cjoin* aux = NULL;

	FirstFila2(&duplas_bloq);
	aux = GetAtIteratorFila2(&duplas_bloq);
	
	while(aux != NULL){

		if(actual_thread->tid == aux->tid_bloqueador){
			if(desbloqueia(aux->thread_bloqueada->tid) == ERRO)
				return ERRO;

			DeleteAtIteratorFila2(&duplas_bloq);
			break;
		} 
		NextFila2(&duplas_bloq);
		aux = GetAtIteratorFila2(&duplas_bloq);

	}

	AppendFila2(&terminados, actual_thread);

	// Tive que manter a lista das threads encerradas para controle do cjoin
	
	/*printf("Desalocando memoria da thread tid %d\n", actual_thread->tid);
	free(actual_thread);
	actual_thread = NULL;*/

	dispatcher();

	return SUCESSO;
}

int existe_tid(int tid){
	TCB_t* aux = NULL;
	
	FirstFila2(&terminados);
	aux = GetAtIteratorFila2(&terminados);
	
	while(aux != NULL){

		if(aux->tid == tid){
			return ERRO;
		} 
		NextFila2(&terminados);
		aux = GetAtIteratorFila2(&terminados);
	}

	printf("Thread #%d bloqueando a execucao.\n", tid);
	return SUCESSO;
}

int ja_bloqueado(int tid){
	info_cjoin* aux = NULL;

	FirstFila2(&duplas_bloq);
	aux = GetAtIteratorFila2(&duplas_bloq);
	
	while(aux != NULL){

		if(aux->tid_bloqueador == tid){
			return ERRO;
		} 
		NextFila2(&duplas_bloq);
		aux = GetAtIteratorFila2(&duplas_bloq);
	}
	return SUCESSO;
}

int desbloqueia(int tid){
	TCB_t* aux = NULL;

	printf("Desbloqueando a thread #%d.\n", tid);

	FirstFila2(&bloqueados);
	aux = GetAtIteratorFila2(&bloqueados);

	while(aux != NULL){
		if(aux->tid == tid){
			aux->state = APTO;

			AppendFila2(&aptos, aux);

			DeleteAtIteratorFila2(&bloqueados);

			return SUCESSO;
		}
		NextFila2(&bloqueados);
		aux = GetAtIteratorFila2(&bloqueados);
	}
	return ERRO;
}
/* 
Funções Principais 
*/

int ccreate (void* (*start)(void*), void *arg){

	if(testa_inicializacao() == -1)
		return ERRO;

	/* Cria a nova thread */
	TCB_t * new_tcb = (TCB_t *) malloc(sizeof(TCB_t));
	if(new_tcb == NULL){
		return ERRO;
	}
	else{
		new_tcb->tid = _next_tid;			
		new_tcb->state = APTO;
		new_tcb->ticket = (Random2() % 256);
		_next_tid++;

		getcontext(&new_tcb->context);	// copia o contexto atual
		new_tcb->context.uc_stack.ss_sp = (char*) malloc(SIGSTKSZ); // 
		new_tcb->context.uc_stack.ss_size = SIGSTKSZ;
		new_tcb->context.uc_link = final_thread;

		makecontext(&new_tcb->context, (void(*)(void))start, 1, arg);

		AppendFila2(&aptos, new_tcb);

		printf("Thread %d criada! Ticket #%d.\n", new_tcb->tid, new_tcb->ticket);

		return new_tcb->tid;
	}
}

int cyield(void){

	if(testa_inicializacao() == -1)
		return ERRO;

	actual_thread->state = APTO;

	printf("cyield: tid %d\n", actual_thread->tid);

	AppendFila2(&aptos, actual_thread);

	swapcontext(&actual_thread->context, dispatcher_c);
	
	return SUCESSO;
}

int cjoin(int tid){
	if(tid >= _next_tid)
		return ERRO;
	else if(tid == actual_thread->tid)
		return ERRO;
	else if(existe_tid(tid) == -1)
		return ERRO;
	else if(ja_bloqueado(tid) == -1)
		return ERRO;
	else{
		info_cjoin * dupla = (info_cjoin*) malloc(sizeof(info_cjoin));
		dupla->tid_bloqueador = tid;
		dupla->thread_bloqueada = actual_thread;
		AppendFila2(&duplas_bloq, dupla);

		printf("Criada dupla: tid_bloqueador #%d e thread bloqueada #%d\n", dupla->tid_bloqueador, actual_thread->tid);

		actual_thread->state = BLOQUEADO;
		AppendFila2(&bloqueados, actual_thread);

		swapcontext(&actual_thread->context, dispatcher_c);
	
		return SUCESSO;
	}
}

int csem_init(csem_t *sem, int count){

	printf("Criando o semaforo.\n");

	if(count < 0)
		return ERRO;

	sem->count = count;
	sem->fila = (FILA2 *) malloc(sizeof(FILA2));
	CreateFila2(sem->fila);

	printf("Criado novo semaforo com contador = %d.\n", count);

	return SUCESSO;
}

int cwait(csem_t *sem){

	if(sem->count > 0){
		(sem->count)--;
		printf("Recurso desbloqueado! Count = %d.\n", sem->count);
		return SUCESSO;
	}
	else{
		printf("Recurso bloqueado! Count = %d.\n", sem->count);

		AppendFila2(sem->fila, actual_thread);
		(sem->count)--;

		actual_thread->state = BLOQUEADO;	

		swapcontext(&actual_thread->context, dispatcher_c);

		return SUCESSO;
	}

}

int csignal(csem_t *sem){
	TCB_t* aux = NULL;

	FirstFila2(sem->fila);
	aux = GetAtIteratorFila2(sem->fila);

	sem->count = sem->count + 1;

	if(aux == NULL){
		printf("Liberando um recurso que nao bloquava ninguem!\n");
		return SUCESSO;
	}
	else{
		printf("Liberando alguem bloqueado na fila.\n");

		aux->state = APTO;
		AppendFila2(&aptos, aux);
		DeleteAtIteratorFila2(sem->fila);
		
		return SUCESSO;
	}
}

int cidentify(char *name, int size)
{

  char aluno1[] = "Camila Primieri - cartao 172662\n";
  char aluno2[] = "Roberta Robert - cartao 194285\n";
  
  if (strlen(aluno1) + strlen(aluno2) > size) {
	  printf("Tentando gravar nomes mais longos do que o permitido");
	  return ERRO;
  }
  
  strcpy(name, aluno1);  // grava o nome do 1o aluno
  strcat(name, aluno2);
  
  return SUCESSO;  

}