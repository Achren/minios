#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/support.h"
#include "../include/cthread.h"

int main() 
{
	int N = 100; // tamanho maximo da string que vai armazenar o nome dos alunos
	char alunosNoGrupo[N];
	
	int ret = cidentify(alunosNoGrupo, N);
	if (ret==-1)
		printf("Nao conseguiu ler os nomes; a funcao tentou gravar uma string mais longe do que %d caracteres\n", N);
	else
		printf("Nomes lidos:\n----\n%s-----\n", alunosNoGrupo);

	return 0;
	exit(0);
}