#include <stdio.h>
#include <stdlib.h>
#include "../include/support.h"
#include "../include/cthread.h"

void* foo1(void *arg){ 
	printf("Executando thread 1.\n");
	return 0;
}

void* foo2(void *arg){ 
	printf("Executando thread 2.\n");
	return 0;
}

void* foo3(void *arg){ 
	printf("Executando thread 3.\n");
	return 0;
}

void* foo4(void *arg){ 
	printf("Executando thread 4.\n");
	return 0;
}

void* foo5(void *arg){ 
	printf("Executando thread 5.\n");
	return 0;
}

int main(){
	int t[5];

	printf("Main antes da criacao das thread\n");

	t[0] = ccreate(foo1, (void *) t[0]);
	if(t[0] == -1)
		printf("Erro criando thread 1.\n");

	t[1] = ccreate(foo2, (void *) t[1]);
	if(t[1] == -1)
		printf("Erro criando thread 2.\n");

	t[2] = ccreate(foo3, (void *) t[2]);
	if(t[2] == -1)
		printf("Erro criando thread 3.\n");

	t[3] = ccreate(foo4, (void *) t[3]);
	if(t[3] == -1)
		printf("Erro criando thread 4.\n");

	t[4] = ccreate(foo5, (void *) t[4]);
	if(t[4] == -1)
		printf("Erro criando thread 5.\n");

	printf("Main apos criacao de thread\n");

	cyield();

	cjoin(1);
	cjoin(2);
	cjoin(3);
	cjoin(4);
	cjoin(5);

	return 0;
	
	exit(0);
}